This directory provides the non-minified versions of the embedded JQuery and
JQuery tablesorter files in the orig source tarball, in order to comply with
Debian Policy. (#788381)

The minified versions of JQuery and JQuery tablesorter for which this
directory provides the non-minified versions are:

    data/tools/addon_manager/jquery.js
    data/tools/addon_manager/tablesorter.js
